const strInput = 'elephant'

function createPalindrome(str) {
  let arrayOfStr = str.split('')
  let arrayOfRes = []
  let strLen = arrayOfStr.length - 1

  for(let i = strLen; i >= 0; i-- ) {
    arrayOfRes[strLen - (strLen - i)] = arrayOfStr[i]
    arrayOfRes[strLen + (strLen - i)] = arrayOfStr[i]
  }
  return arrayOfRes.join('')
}

const result = createPalindrome(strInput)
console.log(result)