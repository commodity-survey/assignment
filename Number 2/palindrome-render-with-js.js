const len = 5

function createDisplayPalindrome(len) {
  let arrayOfRes = []
  let resultLen = len - 1
  for(let i = resultLen; i >= 0; i-- ) {
    let nestedRes = []
    for(let nestedI = 0; nestedI <= (resultLen-i); nestedI++) {
      nestedRes.push('*')
    }
    arrayOfRes[resultLen - (resultLen - i)] = nestedRes.join(' ')
    arrayOfRes[resultLen + (resultLen - i)] = nestedRes.join(' ')
  }

  let maxLen = (len+1) + parseInt((len+1)/2)
  for(let row of arrayOfRes) {
    let addLen = parseInt((maxLen - row.length)/2)
    for(let i = 0; i < addLen; i++) {
      row = ' ' + row
    }
    console.log(row)
  }
}

createDisplayPalindrome(len)