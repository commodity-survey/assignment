const len = 5

function generateNumber(len, max) {
  let res = []
  for(i = 1; i <= max; i++) {
    if(i <= (max - len)) {
      res.push(i)
    } else {
      res.push(' ')
    }
  }
  return res.join('')
}

function createPalindrome(str) {
  let arrayOfStr = str.split('')
  let arrayOfRes = []
  let strLen = arrayOfStr.length - 1

  for(let i = strLen; i >= 0; i-- ) {
    arrayOfRes[strLen - (strLen - i)] = arrayOfStr[i]
    arrayOfRes[strLen + (strLen - i)] = arrayOfStr[i]
  }
  return arrayOfRes
}

function createNestedPalindrome(len) {
  let res = [], resultLen = len - 1
  
  for(let i = resultLen; i >= 0; i-- ) {
    let palLen = resultLen - i
    let palString = generateNumber(palLen, len)
    let nestedRes = createPalindrome(palString).join(' ')

    res[resultLen - (resultLen - i)] = nestedRes
    res[resultLen + (resultLen - i)] = nestedRes
  }
  return res
}

const result = createNestedPalindrome(len)


for(let row of result) {
  console.log(row)
}